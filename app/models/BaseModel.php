<?php

class BaseModel extends Eloquent {
    
    //validates any models' fields
    public static function validate($data) {
        return Validator::make($data, static::$rules);
    }
}