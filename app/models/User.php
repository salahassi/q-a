<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends BaseModel implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

    protected $table = 'users';

    protected $fillable = array('username', 'password');

    public static $rules = array(
        'username' => 'required|unique:users|alpha_dash|min:4',
        'password' => 'required|alpha_num|between:4,8|confirmed',
        'password_confirmation' => 'required|alpha_num|between:4,8'
    );

    public function question() {

    	return $this -> hasMany('Question');
    }

	public function answers() {

		return $this -> hasMany('Answer');
	}
}
