<?php

class Question extends BaseModel {

	protected $table = 'questions';

	protected $fillable = array('question','solved');

	public static $rules = array(
			'question' => 'required|min:10|max:255',
			'solved' => 'in:0,1',
	);

	public function user() {

		return $this -> belongsTo('User');
	}

	public function answers() {

		return $this -> hasMany('Answer');
	}

	public static function unsolved() {

		return static::where('solved','=',0) -> orderBy('id', 'desc') -> paginate(4);
	}

	public static function yourQuestions() {

		return static::where('user_id','=', Auth::user() -> id) -> paginate(4);
	}

	public static function search($k) {
		return static::where('question', 'like', '%'.$k.'%') -> paginate(4);
	}

}
