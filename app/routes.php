<?php

/*
|--------------------------------------------------------------------------
| Routes Filters
|--------------------------------------------------------------------------
*/
Route::filter('preventGuest', function() {
	if(!Auth::check()) return Redirect::route('login') -> with('message', 'You need to login first');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Homepage
Route::get("/", array("as" => "home", "uses" => "QuestionsController@getIndex"));
Route::post("ask", array("as" => "ask", "before" => "csrf", "uses" => "QuestionsController@postCreate"));
Route::post('search', array('before' => 'csrf', 'as' => 'search', 'uses' => 'QuestionsController@postSearch'));
Route::get('result/{keyword}', array('as' => 'results', 'uses' => 'QuestionsController@getResults'));

//Register
Route::get("register", array("as" => "register", "uses" => "UsersController@getNew"));
Route::post("register", array("before" => "csrf", "uses" => "UsersController@postNew"));

//login/logout
Route::get("login", array("as"=>"login", "uses" => "UsersController@getLogin"));
Route::post("login", array("before" => "csrf", "uses" => "UsersController@postLogin"));
Route::get("logout", array("as"=>"logout", "uses" => "UsersController@getLogout"));

//Show question page
Route::get("question/{id}", array("as"=>"question", "uses" => "QuestionsController@getView"));

//Your Questions
Route::get('your-questions', array('as' => 'your_questions', 'uses' => 'QuestionsController@getYourQuestions'));
Route::get('question/{id}/edit', array('as' => 'edit_question', 'uses' => 'QuestionsController@getEdit'));
Route::put('question/update', array("before" => "csrf", 'as' => 'update_question', 'uses' => 'QuestionsController@putUpdate'));

//Answers
Route::post('answer/{id}', array('before' => 'csrf', 'as' => 'answer', 'uses' => 'AnswersController@postCreate'));
