<!DOCTYPE html>
<html>
<head>
    <title>
        {{ $title }}
    </title>
    {{ HTML::style('/css/main.css') }}
</head>
<body>
    <div id="container">
        <div id="header">
            {{ HTML::link("/", "Q&A") }}

            <div id="searchbar">
                {{ Form::open(array ('route' => 'search', 'method' => 'POST')) }}
                {{ Form::text('keyword', '', array('id' => 'keyword', 'placeholder' => 'Search')) }}
                {{ Form::submit('Search') }}
                {{ Form::close() }}
            </div>
        </div>

        <div id="nav">
            <ul>
                <li>{{ HTML::link("/", "Home") }}</li>
                @if(!Auth::check())
                    <li>{{ link_to_route("register", "Register") }}</li>
                    <li>{{ link_to_route("login", "Login") }}</li>
                @else
                	<li>{{ link_to_route('your_questions', "Your Q's") }}</li>
                    <li>{{ link_to_route("logout", "Logout (". Auth::user() -> username .")") }}</li>
                @endif
            </ul>
        </div>

        <div id="content">
            @if(Session::has('message'))
                <p id="message">{{ Session::get('message') }}</p>
            @endif

            @yield('content')
        </div>

        <div id="footer">
            &copy; Q&A {{ date('Y') }}
        </div>
    </div>
</body>
</html>
