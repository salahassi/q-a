@extends('layouts.default')

@section('content')
    <h1>Register</h1>

    @if($errors -> has())
        <p>The following errors have occured:</p>
        <ul id="form-error">
            {{ $errors -> first('username', '<li>:message</li>') }}
            {{ $errors -> first('password', '<li>:message</li>') }}
            {{ $errors -> first('password_confirmation', '<li>:message</li>') }}
        </ul>
    @endif

    {{ Form::open(array('route' => 'register', 'method' => 'POST')) }}

    {{ Form::token() }}
    <p>
        {{ Form::label('username', 'Username') }}
        {{ Form::text('username', Input::old('username')); }}
    </p>

    <p>
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password'); }}
    </p>

    <p>
        {{ Form::label('password_confirmation', 'Confirm Password') }}
        {{ Form::password('password_confirmation'); }}
    </p>

    <p>
        {{ Form::submit('Register'); }}
    </p>

    {{ Form::close() }}
@endsection