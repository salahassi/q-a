@extends('layouts.default')

@section('content')
    <h1>Login</h1>

    {{ Form::open(array('route' => 'login', 'method' => 'POST')) }}

    {{ Form::token() }}
    <p>
        {{ Form::label('Username') }}
        {{ Form::text('username', Input::old('username')) }}
    </p>

    <p>
        {{ Form::label('Password') }}
        {{ Form::password('password') }}
    </p>

    <p>{{ Form::submit() }}</p>

    {{ Form::close() }}
@endsection