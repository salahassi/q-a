@extends('layouts.default')

@section('content')
	<h1>Edit Question</h1>
	
	@if($errors -> has())
		<ul>
			{{ $errors -> first('question', '<li>:message</li>') }}
			{{ $errors -> first('solved', '<li>:message</li>') }}
		</ul>
	@endif
	
	{{ Form::open(array('route'=>'update_question','method' => 'PUT')) }}
	
	
	
	<p>
		{{ Form::label('question',"Question") }}<br/>
    	{{ Form::text('question', $question -> question) }}
	</p>
	
	<p>
		{{ Form::label('solved',"Solved") }}<br/>
		{{ Form::checkbox('solved', 1, $question -> solved) }}
	</p>
	{{ Form::hidden('question_id', $question -> id) }}
	<p>{{ Form::submit('Ask a question') }}</p>
	
	{{ Form::close() }}
@endsection