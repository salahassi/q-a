@extends('layouts.default')

@section('content')
    <h1>Search Results</h1>
    @if(!$questions -> count())
        <p>
            No results found, please try another search.
        </p>
    @else
        <ul>
            @foreach($questions as $question)
                <li>
                    {{ link_to_route('question', $question -> question, $question -> id) }}
                    by {{ ucfirst($question -> user -> username) }}
                </li>
            @endforeach
        </ul>

        {{ $questions -> links() }}
    @endif
@endsection
