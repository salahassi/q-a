@extends('layouts.default')

@section('content')
	<h1>{{ ucfirst(Auth::user() -> username) }} Questions:</h1>
	
	
	@if($questions -> count() > 0)
		<ul>
			@foreach($questions as $qs)
				<li>
					{{ e( $qs -> question ,40) }} - 
					{{ $qs -> solved ? "(Solved) - " : "" }}
					{{ link_to_route('edit_question', 'Edit', $qs -> id) }} - 
					{{ link_to_route('question', 'View', $qs -> id) }}
				</li>
			@endforeach
		</ul>
		
		{{ $questions -> links() }}
	@else
		<p>No questions posted yet!</p>
	@endif
	
@endsection