@extends('layouts.default')

@section('content')
    <div id="ask">
    	<h1>Ask a question</h1>

    	@if(Auth::check())
    		@if($errors -> has())
    			<p>The following errors have accured:</p>
    			<ul id="form-errors">
    				{{ $errors -> first('question', '<li>:message</li>') }}
    			</ul>
    		@endif

    		{{ Form::open(array('route' => 'ask', 'method' => 'POST')) }}
    		{{ Form::token() }}

    		<p>
    			{{ Form::label('question',"Question") }}<br/>
    			{{ Form::text('question', Input::old('question')) }}
    			{{ Form::submit('Ask a question') }}
    		</p>

    		{{ Form::close() }}
    	@else

    		<p>Please login to ask or answer a question.</p>
    	@endif
    </div>
    <hr/>
    <div id="questions">
    	<h1>Unsolved Questions</h1>

    	@if(!$questions -> count())
    		<p>No questions have been asked.</p>
    	@else
    		<ul>
    			@foreach( $questions as $question )
    				<li>
                        {{ link_to_route('question', Str::limit($question -> question, 35), $question -> id) }}...
                        by {{ $question -> user -> username }}
                        ({{ count($question -> answers) }}) {{ Str::plural('Answer', count($question -> answers)) }}
                    </li>
    			@endforeach
    		</ul>

    		{{ $questions -> links() }}
    	@endif
    </div>
@endsection
