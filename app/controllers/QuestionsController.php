<?php

class QuestionsController extends BaseController {

    public function __construct() {
    	$this -> beforeFilter('preventGuest',
    		array(
    			'only'=> array ('postCreate', 'getYourQuestions', 'getEdit', 'putUpdate')
    		)
    	);
    }

    public function getIndex() {
        return View::make('questions.index', array (
        	'title' => 'Home',
        	'questions' => Question::unsolved()
        ));
    }

    public function postCreate() {
//     	var_dump((integer)Auth::user() -> id);
//     	return;
    	$validation = Question::validate(Input::all());

    	if($validation -> passes()) {

    		$question = new Question();
    		$question -> question = Input::get('question');
    		$question -> user() -> associate(Auth::user());
    		$question -> save();

//     		$queries = DB::getQueryLog();
//     		print_r(end($queries));
//     		return;
    		return Redirect::route('home') -> with('message', 'Your question has been posted!');
    	}

    	return Redirect::route('home') ->
    			withErrors($validation) ->
    			withInput();
    }

    public function getView($id) {
    	$question = Question::find($id);

    	return View::make('questions.view', array (
    		'title' => Str::limit($question -> question, 35) . ' - View Question',
    		'question' => $question
    	));
    }

    public function getYourQuestions() {
		return View::make('questions.your_questions', array (
			'title' => 'Your Questions',
			'questions' => Question::yourQuestions()
		));
    }

    public function getEdit($id) {
    	if(!$this -> questionBelongToUser($id))
    		return Redirect::route('your_questions') ->
    			with('message', "You don't have permission to access this page!");

    	$question = Question::find($id);

    	return View::make('questions.edit', array (
    			'title' => 'Edit Question',
    			'question' => $question
    	));
    }

    public function putUpdate() {
    	$id = Input::get('question_id');

    	if(!$this -> questionBelongToUser($id))
    		return Redirect::route('your_questions') ->
    		with('message', "You don't have permission to access this page!");

    	$validation = Question::validate(Input::all());

    	if($validation -> passes()) {

    		$question = Question::find($id);

			$question -> question = Input::get('question');
			$question -> solved = Input::get('solved');

			$question -> save();

			return Redirect::route('edit_question', array($id)) -> with('message', 'Your question has been updated.');
    	}

    	return Redirect::route('edit_question', array($id)) -> withErrors($validation);
    }

    public function postSearch() {
        $k = Input::get('keyword');
        if(empty($k)) {
            return Redirect::route('home') -> with('message', 'Search is empty!');
        }

        return Redirect::route('results', array($k));
    }

    public function getResults($k) {
        return View::make('questions.results', array(
            'title' => 'Search: '. $k,
            'questions' => Question::search($k)
        ));
    }

    private function questionBelongToUser($id) {
    	$question = Question::find($id);

    	if($question -> user_id == Auth::user() -> id) {
    		return true;
    	}

    	return false;
    }
}
