<?php

class AnswersController extends BaseController {

    public $restful = true;

    public function __construct() {
        $this -> beforeFilter('preventGuest',
        array(
            'only'=> array ('postCreate')
            )
        );
    }

    public function postCreate($id) {
        $validation = Answer::validate(Input::all());

        if($validation -> passes()) {
            $answer = new Answer();
            $answer -> user_id = Auth::user() -> id;
            $answer -> question_id = $id;
            $answer -> answer = Input::get('answer');
            $answer -> save();

            return Redirect::route('question', $id) -> with('message', 'Your answer has been posted.');
        }

        return Redirect::route('question', $id) -> withErrors($validation) -> withInput();
    }
}
