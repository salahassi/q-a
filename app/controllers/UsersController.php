<?php

class UsersController extends BaseController {
    
    public $restful = true;
    
    public function getNew() {
        return View::make('users.new', array('title' => 'Register'));   
    }
    
    public function postNew() {
        $validation = User::validate(Input::all());
        echo $validation -> passes();
        if($validation -> passes()) {
            User::create(array(
                "username" => Input::get('username'),
                "password" => Hash::make(Input::get('password'))
            ));
            
            $user = User::whereUsername(Input::get('username')) -> first();
            
            Auth::login($user);
            
            return Redirect::route('home') -> with('message', 'Thank you for registering. You are now logged in!');
        }
        
        return Redirect::route('register') -> withErrors($validation) -> withInput();
    }
    
    public function getLogin() {
        return View::make('users.login', array('title' => 'Login'));   
    }
    
    public function postLogin() {
        $user = array (
            "username" => Input::get('username'),
            "password" => Input::get('password')
        );
        
        if(Auth::attempt($user)) 
            return Redirect::route('home') -> with('message', 'You are logged in!');
        else {
            return Redirect::route('login') -> 
                with('message', 'Username or password is incorrect!') -> 
                withInput();
        }
    }
    
    public function getLogout() {
        if(!Auth::check())
            return Redirect::route('home');
        
        Auth::logout();
        return Redirect::route('login') -> with('message', 'You are now logged out!');
    }
}